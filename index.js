// S28 - Activity 

// Insert a single romm using insertOne method:

db.room.insertOne({
	name : "single",
	accomodates : 2,
	price : 1000,
	description : "A single room with all the basic necessities",
	rooms_available : 10,
	isAvailable : false
});


// Insert a single romm using insertMany method:

db.room.insertMany(
	[
		{
			name : "double",
			accomodates : 3,
			price : 2000,
			description : "A room fit for a small family going on a vacation",
			rooms_available : 5,
			isAvailable : false
		},
		{
			name : "queen",
			accomodates : 4,
			price : 4000,
			description : "A room with a queen sized bed perfect with a simple getaway",
			rooms_available : 15,
			isAvailable : false
		}
	]
);


//Find room with a name double

	db.room.find({name:"double"}); 

// Update using updateOne method

	db.room.updateOne({name:"queen"},{$set:{rooms_available:0}});

// Delete using the deleteMany method

	db.room.deleteMany({rooms_available:0});